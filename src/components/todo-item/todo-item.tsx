import { Component, Prop, Event, EventEmitter, State, Element, Method } from '@stencil/core';


@Component({
  tag: 'todo-item',
  styleUrl: 'todo-item.scss'
})
export class TodoItem {

  @Element() el: HTMLElement;

  @Prop() title: string;
  @Prop() description: string;

  @State() open: boolean = true;

  @Event() todoItemLoaded: EventEmitter;
  @Event() todItemStatusChanged: EventEmitter;

  @Method() isOpen(): boolean {
    return this.open;
  }
  changeItemStatus() {
    this.open = !this.open;
    this.todItemStatusChanged.emit(this.open);
  }

  componentDidLoad() {
    this.todoItemLoaded.emit(this.el);
  }


  render() {
    return (
      <div class="todo-item">
        <input type="checkbox" checked={!this.open} onChange={() => this.changeItemStatus()}/>
        <div class="todo-item-title">{this.title}</div>
        <div class="todo-item-description">{this.description}</div>
        <div class="todo-item-status">{ this.open ? 'open' : 'closed'}</div>
      </div>
    );
  }
}
