import { Component, Element, Listen, State } from '@stencil/core';


@Component({
  tag: 'todo-list',
  styleUrl: 'todo-list.scss'
})
export class TodoList {

  private todoItems: HTMLTodoItemElement[] = [];
  private titleInput: HTMLInputElement;
  private descriptionInput: HTMLInputElement;

  @Element() el: HTMLElement;

  @State() numberOfCompleted: number = 0;
  @State() numberTotal: number = 0;

  @Listen('todoItemLoaded') todoItemLoadedHandler(event: CustomEvent): void {
    this.todoItems.push(event.target as HTMLTodoItemElement);
    this.numberTotal = this.todoItems.length;
    event.stopImmediatePropagation();
  }

  @Listen('todItemStatusChanged') todItemStatusChangedHandler(itemStatus: CustomEvent): void {
    if(itemStatus) {
      this.numberOfCompleted++;
    } else {
      this.numberOfCompleted--;
    }
  }

  addTodoItem(): void {
    const newTodoItem = document.createElement('todo-item');
    newTodoItem.title = this.titleInput.value;
    newTodoItem.description = this.descriptionInput.value;

    this.el.appendChild(newTodoItem);
    this.descriptionInput.value = "";
    this.titleInput.value = "";
  }

  render() {
    return ([
      <form>
        <label>Title:<input type="text" ref={(title:HTMLInputElement) => this.titleInput = title} /></label>
        <label>Description:<input type="text" ref={(description:HTMLInputElement) => this.descriptionInput = description} /></label>
      </form>,
      <button onClick={() => this.addTodoItem()}>New ToDo Item</button>,
      <div class="todo-list-summary">{this.numberOfCompleted} of {this.numberTotal} todos are closed</div>,
      <div class="todo-list"><slot /></div>,
    ]);
  }
}
